var express = require('express');

//var router = express.Router();
var router = require("./router");

var server = require("./server");

var requestHandlers = require("./requestHandlers");

var handle = {};

handle["/"] = requestHandlers.createAsset;

handle["/createAsset"] = requestHandlers.createAsset;
handle["/getAsset"] = requestHandlers.getAsset;
handle["/deleteAsset"] = requestHandlers.deleteAsset;

server.start(router.route, handle);

// TODO: Define app routes.
// implemented separately




module.exports = router;
